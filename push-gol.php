<?php

require 'vendor/autoload.php';
require 'conway.php';

// Array to access pads
$pads = [
    range(92, 99),
    range(84, 91),
    range(76, 83),
    range(68, 75),
    range(60, 67),
    range(52, 59),
    range(44, 51),
    range(36, 43),
];

// Same but reversed
$range = range(36, 99);
$r_pads = [];
foreach ($range as $v)
{
    // Top left origin
    $r_pads[sprintf('%02X',$v)] = [-((int)floor(($v - 36)/8))+7 ,($v - 36) % 8];
}
unset($range);


// Turn on arrows
midi('B02C7F');
midi('B02D7F');
midi('B02E7F');
midi('B02F7F');
midi('B0367F');
midi('B0377F');
midi('B03E7F');
midi('B03F7F');
midi('B0557E');
midi('B0567F');

// Format world
$width  = 64;
$height = 64;

$world = [];

reset_world();

// Define a starting glider
$world[7][9] = 1;
$world[8][7] = 1;
$world[8][9] = 1;
$world[9][8] = 1;
$world[9][9] = 1;

// Run
$yoff = 28;
$xoff = 28;

push_world($world);

// Listen push midi port
// (me way get rid of stdin?)
$cmd = "amidi -p hw:4,0,0 -d";
$descriptorspec = array(
//    0 => array("pipe", "r"),   // stdin is a pipe that the child will read from
   1 => array("pipe", "w"),   // stdout is a pipe that the child will write to
//    2 => array("pipe", "w")    // stderr is a pipe that the child will write to
);
flush();
$process = proc_open($cmd, $descriptorspec, $pipes, realpath('./'), array());

if (is_resource($process)) {
    $s = '';
    // Read chars by chars midi output
    while (false !== ($c = fgetc($pipes[1])) ) {

        $s .= $c;
        // Each 3 bytes (+\n)
        if (strlen($s) == 9)
        {
            $r = explode(' ', $s);
            $s = '';
            
            $r[0] = trim($r[0]);
            $absy = $r_pads[$r[1]][0] + $yoff;
            $absx = $r_pads[$r[1]][1] + $xoff;

            // Pads input
            if ($r[0] == '90')
            {
                if ($world[$absy][$absx])
                {
                    // midi('90'.$r[1].'60');
                    $world[$absy][$absx] = false;
                }
                else
                {
                    // midi('90'.$r[1].'33');
                    $world[$absy][$absx] = true;
                }

            }
            // Controllers input
            elseif ($r[0] == 'B0' && $r[2] != '00')
            {
                switch($r[1])
                {
                    case '2D':
                        if ($xoff < $width - 8)
                            $xoff++;
                        break;
                    case '2C':
                        if ($xoff > 0)
                            $xoff--;
                        break;
                    case '2E':
                        if ($yoff > 0)
                            $yoff--;
                        break;
                    case '2F':
                        if ($yoff < $height - 8)
                            $yoff++;
                        break;
                    case '3F':
                        if ($xoff < $width - 8 - 5)
                            $xoff += 5;
                        break;
                    case '3E':
                        if ($xoff > 5)
                            $xoff -= 5;
                        break;
                    case '37':
                        if ($yoff > 5)
                            $yoff -= 5;
                        break;
                    case '36':
                        if ($yoff < $height - 8 - 5)
                            $yoff += 5;
                        break;
                    case '55':
                        $world = tick($world);
                        break;
                    case '56':
                        reset_world();
                        break;
                }
            }
            push_world($world);
            print_world($world);
        }
        flush();
    }
}

exit;

// Functions

function midi($bytes)
{
    // Set your midi hardware here
	exec('amidi -p hw:4,0,0 -S '.$bytes.' 2> /dev/null &');
	usleep('600');
}

function cell($add, $val)
{
    midi('90'.$add. match($val){
        0 => '36',
        1 => '16',
        2 => '30',
        3 => '60'
    });
}

function reset_world()
{
    global $world, $height, $width;

    foreach (range(0, $height-1) as $y) {
        foreach (range(0, $width-1) as $x) {
            $world[$y][$x] = 0;
        }
    }
}

function push_world($world)
{
    global $pads, $xoff, $yoff, $width, $height, $prev;
    $y = 0;
    foreach ($pads as $l)
    {
        $x = 0;
        foreach ($l as $pad)
        {
            $cell = sprintf('%02X', $pad);
            $absy = $y + $yoff;
            $absx = $x + $xoff;
            $state = $world[$absy][$absx];
            if ($absy == 0 OR $absy == $width - 1 OR $absx == 0 OR $absx == $height - 1)
            {   
                $col = $state ? 0:1;
                if (!isset($prev[$y][$x]) OR $col !== $prev[$y][$x])
                {
                    cell($cell, $col);
                }
                $prev[$y][$x] = $col;
            }
            else
            {
                $col = $state ? 2:3;
                if (!isset($prev[$y][$x]) OR $col !== $prev[$y][$x])
                {
                    cell($cell, $col);
                }                
                $prev[$y][$x] = $col;
            }
            $x++;
        }
        $y++;
    }
}